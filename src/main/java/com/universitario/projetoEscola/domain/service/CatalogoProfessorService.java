/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.universitario.projetoEscola.domain.service;
import com.universitario.projetoEscola.domain.exception.NegocioException;
import com.universitario.projetoEscola.domain.model.Professor;

import com.universitario.projetoEscola.domain.repository.ProfessorRepository;
import lombok.AllArgsConstructor;


import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jose
 */
@AllArgsConstructor
@Service
public class CatalogoProfessorService {
    
    private ProfessorRepository professorRepository;
    
    @Transactional
    public Professor salvar(Professor prof){
        boolean cpfEmUso = professorRepository.findByCpf(prof.getCpf())
        .stream()
        .anyMatch(profEncontrado -> !profEncontrado.equals(prof));     
        
        if(cpfEmUso && prof.getId().isEmpty())
            throw new NegocioException("Já existe um professor com este CPF");
        
        return professorRepository.save(prof);
    }

    @Transactional
    public Professor atualizar(Professor prof){
        return professorRepository.save(prof);    
    }

    
    @Transactional
    public void excluir(String professorId){
        professorRepository.deleteByCpf(professorId);
    }
}
