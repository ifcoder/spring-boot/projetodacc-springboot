/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.universitario.projetoEscola.domain.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 *
 * @author jose
 */

@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Professor {
    @EqualsAndHashCode.Include
    @Id
    private String id;

    @NotBlank
    @Size(max = 80)
    private String nome;
    private char sexo;
    private int idade;
    private String cpf;
    
    

    @Override
    public String toString() {
        String ttt = "{ id:"+id
                    +", nome:"+nome
                    +", sexo:"+ sexo
                    +", idade:"+ idade
                    +", cpf:"+ cpf
                    +"}";
        return ttt;
    }
}
