package com.universitario.projetoEscola.domain.repository;

import com.universitario.projetoEscola.domain.model.Professor;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfessorRepository extends JpaRepository<Professor, String> {
    
    Optional<Professor> findByCpf(String cpf);
    void deleteByCpf(String cpf);
}
