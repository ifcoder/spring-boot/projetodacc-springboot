package com.universitario.projetoEscola.api.exceptionhandler;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.universitario.projetoEscola.api.exceptionhandler.Problema.Campo;
import com.universitario.projetoEscola.domain.exception.NegocioException;

import java.util.ArrayList;
import org.springframework.validation.FieldError;

/*  Aula 2.6
    @ControllerAdvice    
 * Diz que a classe é um componente do spring porem com um propósito de tratar exceções de forma global.
 * Para todo o Controller da aplicacao, que deparar com uma exception chamará esta classe Exception
 * 
 * extends ResponseEntityExceptionHandler:
 * Vamos herdar a maioria dos tratamentos e so sobreescrever aqueles que desejarmos
 * Quando fazemos esta herança neste momento se fizemos um POST errado ja vai "tratar" a exceção
 * o resquest virá vazio! O que não é uma boa prática, afinal o erro 400 é do lado do cliente e 
 * nao tem nada orientando ele sobre seu erro.
 * Por isso devemos fazer o override no metodo referido
 */
@ControllerAdvice 
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
            HttpHeaders headers, HttpStatus status, WebRequest request) {
        
        List<Campo> campos = new ArrayList<>();
        for(ObjectError error: ex.getBindingResult().getAllErrors()){
            String nome = ((FieldError) error).getField();
            String mensagem = error.getDefaultMessage();
            
            campos.add(new Problema.Campo(nome, mensagem));
        }
                
        Problema problema = new Problema();
        problema.setStatus(status.value());
        problema.setDataHora(LocalDateTime.now());
        problema.setTitulo("Um ou mais campos inválidos. Preencha corretamente.");
        problema.setCampos(campos);

        return handleExceptionInternal(ex, problema, headers, status, request);
    }

    /*
     * @ExceptionHandler, esta anotation vai capturar todas as exceptions lancadas pela classe NegocioException
     * de nossa API
     * 
     * O Parametro WebRequest será colocado automaticamente pelo SPRING para gente. Ele sera usado 
     * no handleExceptionInternal
     */
    @ExceptionHandler(NegocioException.class)
    public ResponseEntity<Object> handleNegocio(NegocioException ex, WebRequest request){

        //Vamos criar um body para entregar para handeExceptionInternal
        //Para isso usaremos nossa classe problema.
        Problema problema = new Problema();
        //Neste exemplo nao temos o status como parametro. Logo criaremos um.
        //Neste caso o mais adequado é um erro da familia 400
        HttpStatus status = HttpStatus.BAD_REQUEST;
        problema.setStatus(status.value());
        problema.setDataHora(LocalDateTime.now());
        problema.setTitulo(ex.getMessage());
        //Neste caso nao temos campos, por isso vamos colocar a anotation 
        //@JsonInclude(JsonInclude.Include.NON_NULL) na classe Problema
        //pois ai nao aparece este campo la no JSON
        //problema.setCampos(campos);

        
        //handleExceptionInternal retorna um ResponseEntity
        return handleExceptionInternal(ex, problema, new HttpHeaders(), status, request);
    }
}
