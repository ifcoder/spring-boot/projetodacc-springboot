/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.universitario.projetoEscola.api.controller;

import com.universitario.projetoEscola.domain.model.Professor;
import com.universitario.projetoEscola.domain.repository.ProfessorRepository;
import com.universitario.projetoEscola.domain.service.CatalogoProfessorService;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
//import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;

/**
 *
 * @author jose
 */

@AllArgsConstructor
@RestController
@RequestMapping("/professores")//se nao usar esta linha tem que colocar /professores em todos os mapping la embaixo
public class ProfessorController {
    
    private ProfessorRepository professorRepository; 
    private CatalogoProfessorService catalogoProfessorService;

    @GetMapping("/")
    public List<Professor> list() {
        return professorRepository.findAll();        
    }

    @GetMapping("/{cpf}")
    public ResponseEntity<Professor> buscar(@PathVariable String cpf) {
        return professorRepository.findByCpf(cpf)
        .map(professor -> ResponseEntity.ok(professor))
        .orElse(ResponseEntity.notFound().build());
        
        // outra forma de fazer:
        // Optional<Professor> profFound = professorRepository.findById(id);        

        // if (profFound.isPresent())
        //     return ResponseEntity.ok(profFound.get());
        // else
        //     return ResponseEntity.notFound().build();
    }

    @PutMapping("/{cpf}")
    public ResponseEntity<Professor> update(@PathVariable String cpf, @Valid @RequestBody Professor professor) {
        
        Optional<Professor> profFound = professorRepository.findByCpf(cpf);
        
        if(!profFound.isPresent())
            return ResponseEntity.notFound().build();
        else
            professor.setId(profFound.get().getId());
        
        professor = catalogoProfessorService.salvar(professor);
        return ResponseEntity.ok(professor);
    }


    @PostMapping("/")
    @ResponseStatus(HttpStatus.CREATED)
    public Professor add(@Valid  @RequestBody Professor prof) {
        UUID uuid = UUID.randomUUID();
        prof.setId(uuid.toString());
        return catalogoProfessorService.salvar(prof);
    }

    @DeleteMapping("/{cpf}")
    public ResponseEntity<Void> delete(@PathVariable String cpf) {        
        if(!professorRepository.findByCpf(cpf).isPresent())
            return ResponseEntity.notFound().build();
                
        catalogoProfessorService.excluir(cpf);
        return ResponseEntity.noContent().build();
    }
}
