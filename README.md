# ProjetoDACC - Spring Boot

Este projeto é uma API REST desenvolvida com Spring Boot para gerenciar informações de professores. Ele oferece operações CRUD (Create, Read, Update, Delete) para gerenciar dados de professores, incluindo nome, sexo, idade e CPF.

## Índice

- [ProjetoDACC - Spring Boot](#projetodacc---spring-boot)
  - [Índice](#índice)
  - [Funcionalidades](#funcionalidades)
  - [Tecnologias Utilizadas](#tecnologias-utilizadas)
  - [Como Executar](#como-executar)
  - [Endpoints](#endpoints)
  - [Contribuições](#contribuições)
  - [Licença](#licença)

## Funcionalidades

- Listar todos os professores.
- Buscar professor pelo CPF.
- Adicionar um novo professor.
- Atualizar informações de um professor existente.
- Deletar um professor pelo CPF.

## Tecnologias Utilizadas

- **Spring Boot**: Framework Java para criar aplicações web e microserviços.
- **Spring Data JPA**: Facilita o acesso a dados em aplicações Java Persistence API.
- **Lombok**: Biblioteca Java que ajuda a reduzir o código boilerplate.
- **H2 Database**: Banco de dados em memória para desenvolvimento e testes.

## Como Executar

1. Clone o repositório:
   ```bash
   git clone [URL_DO_REPOSITÓRIO]
   ```
2. Navegue até o diretório do projeto:
   ```bash
   cd [DIRETÓRIO_DO_PROJETO]
   ```
3. Execute o projeto com o Maven:
   ```bash
   ./mvnw spring-boot:run
   ```
4. A API estará disponível em: `http://localhost:8080/professores`

## Endpoints

- **GET** `/professores/`: Lista todos os professores.
- **GET** `/professores/{cpf}`: Busca um professor pelo CPF.
- **POST** `/professores/`: Adiciona um novo professor.
- **PUT** `/professores/{cpf}`: Atualiza um professor existente pelo CPF.
- **DELETE** `/professores/{cpf}`: Deleta um professor pelo CPF.

## Contribuições

Contribuições são sempre bem-vindas! Sinta-se à vontade para abrir uma issue ou enviar um pull request.

## Licença

Este projeto está sob a licença MIT. Veja o arquivo [LICENSE](LICENSE) para mais detalhes.
